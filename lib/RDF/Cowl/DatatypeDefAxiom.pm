package RDF::Cowl::DatatypeDefAxiom;
# ABSTRACT: Represents a DatatypeDefinition axiom in the OWL 2 specification

# CowlDatatypeDefAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DatatypeDefAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DatatypeDefAxiom

=cut
