package RDF::Cowl::Primitive;
# ABSTRACT: Represents a primitive, a collective term for entities, anonymous individuals, and IRIs

# CowlPrimitive
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Primitive unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Primitive

=cut
