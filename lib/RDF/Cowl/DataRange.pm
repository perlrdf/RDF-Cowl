package RDF::Cowl::DataRange;
# ABSTRACT: Represents a DataRange in the OWL 2 specification

# CowlDataRange
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataRange unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataRange

=cut
