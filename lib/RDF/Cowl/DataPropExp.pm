package RDF::Cowl::DataPropExp;
# ABSTRACT: Represents a DataPropertyExpression in the OWL 2 specification

# CowlDataPropExp
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataPropExp unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataPropExp

=cut
