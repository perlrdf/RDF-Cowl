package RDF::Cowl::HasKeyAxiom;
# ABSTRACT: Represents a HasKey axiom in the OWL 2 specification

# CowlHasKeyAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::HasKeyAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc HasKeyAxiom

=cut
