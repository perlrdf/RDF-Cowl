package RDF::Cowl::DataHasValue;
# ABSTRACT: Represents DataHasValue in the OWL 2 specification

# CowlDataHasValue
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataHasValue unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataHasValue

=cut
