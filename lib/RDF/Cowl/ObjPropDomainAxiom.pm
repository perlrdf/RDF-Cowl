package RDF::Cowl::ObjPropDomainAxiom;
# ABSTRACT: Represents an ObjectPropertyDomain axiom in the OWL 2 specification

# CowlObjPropDomainAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjPropDomainAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjPropDomainAxiom

=cut
