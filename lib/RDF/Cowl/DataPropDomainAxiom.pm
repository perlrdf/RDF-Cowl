package RDF::Cowl::DataPropDomainAxiom;
# ABSTRACT: Represents a DataPropertyDomain axiom in the OWL 2 specification

# CowlDataPropDomainAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataPropDomainAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataPropDomainAxiom

=cut
