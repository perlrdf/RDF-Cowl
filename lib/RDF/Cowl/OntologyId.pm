package RDF::Cowl::OntologyId;
# ABSTRACT: An object that identifies an ontology

# CowlOntologyId
use strict;
use warnings;
use namespace::autoclean;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Optional);
use Type::Params -sigs;
use Class::Method::Modifiers qw(around after);

use FFI::Platypus::Record;

my $ffi = RDF::Cowl::Lib->ffi;

record_layout_1($ffi,
	opaque => '_iri', # CowlIRI*
	opaque => '_version', # CowlIRI*
);
$ffi->type('record(RDF::Cowl::OntologyId)', 'CowlOntologyId');

around new => sub {
	my $orig  = shift;
	my $class = shift;
	my $ret = $orig->($class);

	state $signature = signature (
		named => [
			iri      => Optional[CowlIRI],
			version  => Optional[CowlIRI],
		],
	);

	my ( $args ) = $signature->(@_);

	$ret->_iri( $ffi->cast( 'CowlIRI' => 'opaque', $args->iri ) )
		if $args->has_iri;

	$ret->_version( $ffi->cast( 'CowlIRI' => 'opaque', $args->version ) )
		if $args->has_version;

	$ret;
};


sub iri {
	my ($self) = @_;
	return $ffi->cast('opaque', 'CowlIRI', $self->_iri )
}

sub version {
	my ($self) = @_;
	return $ffi->cast('opaque', 'CowlIRI', $self->_version )
}

require RDF::Cowl::Lib::Gen::Class::OntologyId unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc OntologyId

=cut
