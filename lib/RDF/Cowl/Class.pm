package RDF::Cowl::Class;
# ABSTRACT: Represents a Class in the OWL 2 specification

# CowlClass
use strict;
use warnings;
use parent qw(RDF::Cowl::ClsExp RDF::Cowl::Entity);
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Class unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Class

=cut
