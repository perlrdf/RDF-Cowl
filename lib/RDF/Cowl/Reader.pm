package RDF::Cowl::Reader;
# ABSTRACT: Defines a reader

# CowlReader
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);
use FFI::Platypus::Record;

my $ffi = RDF::Cowl::Lib->ffi;
record_layout_1($ffi,
	# char const *name;
	'string' => 'name',

	# cowl_ret (*read)(UIStream *istream, CowlIStream *stream);
	opaque => 'read',
);
$ffi->type('record(RDF::Cowl::Reader)', 'CowlReader');

1;
