package RDF::Cowl::DataRangeType;
# ABSTRACT: Represents the type of CowlDataRange

# CowlDataRangeType
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum CowlDataRangeType
# From <cowl_data_range_type.h>

my @ENUM_CODES = qw(
    COWL_DRT_DATATYPE
    COWL_DRT_DATATYPE_RESTR
    COWL_DRT_DATA_INTERSECT
    COWL_DRT_DATA_UNION
    COWL_DRT_DATA_COMPL
    COWL_DRT_DATA_ONE_OF
);
my @_COWL_DRT_CODE =
	map {
		[ $ENUM_CODES[$_] =~ s/^COWL_DRT_//r , $_ ],
	} 0..$#ENUM_CODES;

$ffi->load_custom_type('::Enum', 'CowlDataRangeType',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_DRT_CODE,
);


1;
