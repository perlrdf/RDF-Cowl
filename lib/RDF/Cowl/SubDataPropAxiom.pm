package RDF::Cowl::SubDataPropAxiom;
# ABSTRACT: Represents a SubDataPropertyOf axiom in the OWL 2 specification

# CowlSubDataPropAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::SubDataPropAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc SubDataPropAxiom

=cut
