package RDF::Cowl::AnnotProp;
# ABSTRACT: Represents an AnnotationProperty in the OWL 2 specification

# CowlAnnotProp
use strict;
use warnings;
use parent 'RDF::Cowl::Entity';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::AnnotProp unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc AnnotProp

=cut
