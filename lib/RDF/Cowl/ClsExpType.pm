package RDF::Cowl::ClsExpType;
# ABSTRACT: Represents the type of CowlClsExp

# CowlClsExpType
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum CowlClsExpType
# From <cowl_cls_exp_type.h>

my @ENUM_CODES = qw(
    COWL_CET_CLASS
    COWL_CET_OBJ_SOME
    COWL_CET_OBJ_ALL
    COWL_CET_OBJ_MIN_CARD
    COWL_CET_OBJ_MAX_CARD
    COWL_CET_OBJ_EXACT_CARD
    COWL_CET_OBJ_HAS_VALUE
    COWL_CET_OBJ_HAS_SELF
    COWL_CET_DATA_SOME
    COWL_CET_DATA_ALL
    COWL_CET_DATA_MIN_CARD
    COWL_CET_DATA_MAX_CARD
    COWL_CET_DATA_EXACT_CARD
    COWL_CET_DATA_HAS_VALUE
    COWL_CET_OBJ_INTERSECT
    COWL_CET_OBJ_UNION
    COWL_CET_OBJ_COMPL
    COWL_CET_OBJ_ONE_OF
);
my @_COWL_CET_CODE =
	map {
		[ $ENUM_CODES[$_] =~ s/^COWL_CET_//r , $_ ],
	} 0..$#ENUM_CODES;

$ffi->load_custom_type('::Enum', 'CowlClsExpType',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_CET_CODE,
);


1;
