package RDF::Cowl::DataPropRangeAxiom;
# ABSTRACT: Represents a DataPropertyRange axiom in the OWL 2 specification

# CowlDataPropRangeAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataPropRangeAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataPropRangeAxiom

=cut
