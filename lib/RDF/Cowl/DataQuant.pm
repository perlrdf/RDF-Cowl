package RDF::Cowl::DataQuant;
# ABSTRACT: Represents DataSomeValuesFrom and DataAllValuesFrom in the OWL 2 specification

# CowlDataQuant
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataQuant unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataQuant

=cut
