package RDF::Cowl::DataPropAssertAxiom;
# ABSTRACT: Represents a DataPropertyAssertion or NegativeDataPropertyAssertion axiom in the OWL 2 specification

# CowlDataPropAssertAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataPropAssertAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataPropAssertAxiom

=cut
