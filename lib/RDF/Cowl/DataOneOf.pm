package RDF::Cowl::DataOneOf;
# ABSTRACT: Represents DataOneOf in the OWL 2 specification

# CowlDataOneOf
use strict;
use warnings;
use parent 'RDF::Cowl::DataRange';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataOneOf unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataOneOf

=cut
