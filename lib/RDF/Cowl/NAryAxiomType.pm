package RDF::Cowl::NAryAxiomType;
# ABSTRACT: Represents the type of CowlNAryClsAxiom, CowlNAryObjPropAxiom, CowlNAryDataPropAxiom and CowlNAryIndAxiom

# CowlNAryAxiomType
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum CowlNAryAxiomType
# From <cowl_nary_axiom_type.h>

my @ENUM_CODES = qw(
    COWL_NAT_EQUIV
    COWL_NAT_DISJ
);
my @_COWL_NAT_CODE =
	map {
		[ $ENUM_CODES[$_] =~ s/^COWL_NAT_//r , $_ ],
	} 0..$#ENUM_CODES;

$ffi->load_custom_type('::Enum', 'CowlNAryAxiomType',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_NAT_CODE,
);


1;
