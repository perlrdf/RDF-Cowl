package RDF::Cowl::ObjCompl;
# ABSTRACT: Represents ObjectComplementOf in the OWL 2 specification

# CowlObjCompl
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjCompl unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjCompl

=cut
