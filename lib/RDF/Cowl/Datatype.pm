package RDF::Cowl::Datatype;
# ABSTRACT: Represents a Datatype in the OWL 2 specification

# CowlDatatype
use strict;
use warnings;
use parent qw(RDF::Cowl::DataRange RDF::Cowl::Entity);
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Datatype unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Datatype

=cut
