package RDF::Cowl::Writer;
# ABSTRACT: Defines a writer

# CowlWriter
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);
use FFI::Platypus::Record;

my $ffi = RDF::Cowl::Lib->ffi;
record_layout_1($ffi,
#typedef struct CowlWriter {
	# char const *name;
	'string' => 'name',

	# cowl_ret (*write_ontology)(UOStream *stream, CowlOntology *ontology);
	opaque => 'write_ontology',

	# cowl_ret (*write)(UOStream *stream, CowlAny *object);
	opaque => 'write',

	# CowlStreamWriter stream;
	"char[@{[ $ffi->sizeof('CowlStreamWriter') ]}]"
	               => 'stream',
);
$ffi->type('record(RDF::Cowl::Writer)', 'CowlWriter');

1;
