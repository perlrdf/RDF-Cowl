package RDF::Cowl::Ulib::UStrBuf;
# ABSTRACT: [Internal] A mutable string buffer

# UStrBuf
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::UStrBuf unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc UStrBuf

=cut
