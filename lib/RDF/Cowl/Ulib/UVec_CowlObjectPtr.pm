package RDF::Cowl::Ulib::UVec_CowlObjectPtr;
# ABSTRACT: [Internal] Raw vector

# UVec(CowlObjectPtr)
# See also: CowlVector
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

# CowlAny
# -> same as CowlObjectPtr
# -> same as CowlObject *

unless( $RDF::Cowl::no_gen ) {
	# uvec_CowlObjectPtr
	$ffi->attach( [
	 "COWL_WRAP_my_uvec_new_on_heap_CowlObjectPtr"
	 => "new" ] =>
		[
		],
		=> "UVec_CowlObjectPtr"
	);
}

require RDF::Cowl::Lib::Gen::Class::UVec_CowlObjectPtr unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc UVec_CowlObjectPtr

=cut
