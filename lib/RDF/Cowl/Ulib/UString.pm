package RDF::Cowl::Ulib::UString;
# ABSTRACT: [Internal] A counted String

# UString
# See also: CowlString
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);
use Class::Method::Modifiers qw(around);
use FFI::Platypus::Record;
use FFI::Platypus::Memory qw( strdup strcpy );

my $ffi = RDF::Cowl::Lib->ffi;

# UString
record_layout_1($ffi,
	'ulib_uint' => '_size',
	'opaque'    => '_data',
);
$ffi->type('record(RDF::Cowl::Ulib::UString)', 'UString');

around new => sub {
	my ($orig, $class, $arg) = @_;
	return RDF::Cowl::Ulib::UString::copy_buf($arg);
};

require RDF::Cowl::Lib::Gen::Class::UString unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc UString

=cut
