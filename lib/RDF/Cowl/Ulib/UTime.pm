package RDF::Cowl::Ulib::UTime;
# ABSTRACT: [Internal] Date and time

# UTime
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::UTime unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc UTime

=cut
