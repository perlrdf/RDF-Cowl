package RDF::Cowl::Ulib::UVersion;
# ABSTRACT: [Internal] Version information

# UVersion
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::UVersion unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc UVersion

=cut
