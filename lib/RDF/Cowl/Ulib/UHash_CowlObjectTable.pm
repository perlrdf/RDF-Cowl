package RDF::Cowl::Ulib::UHash_CowlObjectTable;
# ABSTRACT: [Internal] Raw hash table

# UHash(CowlObjectTable)
# See also: CowlTable
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::UHash_CowlObjectTable unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc UHash_CowlObjectTable

=cut
