package RDF::Cowl::Ulib::UIStream;
# ABSTRACT: [Internal] Models an input stream

# UIStream
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::UIStream unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc UIStream

=cut
