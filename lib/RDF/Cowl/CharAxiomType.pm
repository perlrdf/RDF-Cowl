package RDF::Cowl::CharAxiomType;
# ABSTRACT: Represents the type of CowlObjPropCharAxiom

# CowlCharAxiomType
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum CowlCharAxiomType
# From <cowl_char_axiom_type.h>

my @ENUM_CODES = qw(
    COWL_CAT_FUNC
    COWL_CAT_INV_FUNC
    COWL_CAT_SYMM
    COWL_CAT_ASYMM
    COWL_CAT_TRANS
    COWL_CAT_REFL
    COWL_CAT_IRREFL
);
my @_COWL_CAT_CODE =
	map {
		[ $ENUM_CODES[$_] =~ s/^COWL_CAT_//r , $_ ],
	} 0..$#ENUM_CODES;

$ffi->load_custom_type('::Enum', 'CowlCharAxiomType',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_CAT_CODE,
);


1;
