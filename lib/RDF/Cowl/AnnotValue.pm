package RDF::Cowl::AnnotValue;
# ABSTRACT: Represents annotation values, which can be either anonymous individuals, IRIs, or literals

# CowlAnnotValue
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::AnnotValue unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc AnnotValue

=cut
