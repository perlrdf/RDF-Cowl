package RDF::Cowl::InvObjProp;
# ABSTRACT: Represents an InverseObjectProperty in the OWL 2 specification

# CowlInvObjProp
use strict;
use warnings;
use parent 'RDF::Cowl::ObjPropExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::InvObjProp unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc InvObjProp

=cut
