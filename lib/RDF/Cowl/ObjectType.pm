package RDF::Cowl::ObjectType;
# ABSTRACT: Represents the type of CowlObject

# CowlObjectType
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum CowlObjectType
# From <cowl_object_type.h>

our ($_ENUM_CODES, $_ENUM_TYPES);

require RDF::Cowl::Lib::Gen::Enum::ObjectType unless $RDF::Cowl::no_gen;

my @_COWL_OT_CODE =
	map {
		[ $_ENUM_CODES->[$_] =~ s/^COWL_OT_//r , $_ ],
	} 0..$_ENUM_CODES->$#*;

$ffi->load_custom_type('::Enum', 'CowlObjectType',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_OT_CODE,
);


1;
