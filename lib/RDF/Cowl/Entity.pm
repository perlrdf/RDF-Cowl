package RDF::Cowl::Entity;
# ABSTRACT: Represents an Entity in the OWL 2 specification

# CowlEntity
use strict;
use warnings;
use parent 'RDF::Cowl::Primitive';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

##############################
# * @note Only available if COWL_ENTITY_IDS is defined.
#
$ffi->ignore_not_found(1);

$ffi->attach( [ "cowl_entity_get_id" => "get_id" ] =>
	[
		arg "CowlAnyEntity" => "entity",
	],
	=> "ulib_uint"
);

$ffi->attach( [ "cowl_entity_with_id" => "with_id" ] =>
	[
		arg "ulib_uint" => "id",
	],
	=> "CowlAnyEntity"
);

$ffi->ignore_not_found(0);
##############################

require RDF::Cowl::Lib::Gen::Class::Entity unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Entity

=cut
