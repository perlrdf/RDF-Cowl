package RDF::Cowl::FuncDataPropAxiom;
# ABSTRACT: Represents a FunctionalDataProperty axiom in the OWL 2 Specification

# CowlFuncDataPropAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::FuncDataPropAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc FuncDataPropAxiom

=cut
