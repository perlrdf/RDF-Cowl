# PODNAME: RDF::Cowl::Lib::Gen::Class::Annotation
# ABSTRACT: Generated docs for RDF::Cowl::Annotation

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::Annotation>

=cut


=construct new


B<Signature>:

  RDF::Cowl::Annotation->new(CowlAnyAnnotValue $value, Maybe[ CowlVector ] $annot)


B<Params>:

  CowlAnnotProp $prop 
  CowlAnyAnnotValue $value 
  Maybe[ CowlVector ] $annot I<[optional]>


Documentation:


  Returns a retained annotation.
  
  @param prop The annotation property.
  @param value The annotation value.
  @param annot [optional] The annotations.
  @return Retained annotation. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_annotation >>

=back

=cut



=method get_prop


B<Signature>:

  
  $self->get_prop()


B<Params>:

  CowlAnnotation $annot 


Documentation:


  Gets the annotation property.
  
  @param annot The annotation.
  @return The annotation property.



=over 2

B<C function name>: C<< cowl_annotation_get_prop >>

=back

=cut



=method get_value


B<Signature>:

  
  $self->get_value()


B<Params>:

  CowlAnnotation $annot 


Documentation:


  Gets the annotation value.
  
  @param annot The annotation.
  @return The annotation value.



=over 2

B<C function name>: C<< cowl_annotation_get_value >>

=back

=cut



=method get_annot


B<Signature>:

  
  $self->get_annot()


B<Params>:

  CowlAnnotation $annot 


Documentation:


  Gets the annotations of the specified annotation.
  
  @param annot The annotation.
  @return The annotations.



=over 2

B<C function name>: C<< cowl_annotation_get_annot >>

=back

=cut

