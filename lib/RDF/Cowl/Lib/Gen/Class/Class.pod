# PODNAME: RDF::Cowl::Lib::Gen::Class::Class
# ABSTRACT: Generated docs for RDF::Cowl::Class

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::Class>

=cut


=construct new


B<Signature>:

  RDF::Cowl::Class->new()


B<Params>:

  CowlIRI $iri 


Documentation:


  Returns a retained class.
  
  @param iri IRI of the class.
  @return Retained class. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_class >>

=back

=cut


=construct from_string


B<Signature>:

  RDF::Cowl::Class->from_string()


B<Params>:

  UString $string 


Documentation:


  Returns a retained class given the string representation of its IRI.
  
  @param string String representation of the IRI.
  @return Retained class. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_class_from_string >>

=back

=cut



=method get_iri


B<Signature>:

  
  $self->get_iri()


B<Params>:

  CowlClass $cls 


Documentation:


  Gets the IRI of the specified class.
  
  @param cls The class.
  @return IRI of the class.



=over 2

B<C function name>: C<< cowl_class_get_iri >>

=back

=cut

