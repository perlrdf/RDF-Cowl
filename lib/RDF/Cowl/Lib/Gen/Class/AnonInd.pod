# PODNAME: RDF::Cowl::Lib::Gen::Class::AnonInd
# ABSTRACT: Generated docs for RDF::Cowl::AnonInd

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::AnonInd>

=cut


=construct new


B<Signature>:

  RDF::Cowl::AnonInd->new()


B<Params>:

  CowlString $id 


Documentation:


  Returns a retained anonymous individual.
  
  @param id Anonymous individual identifier.
  @return Retained anonymous individual. Throws exception on error.
  
  @note By passing NULL as the identifier, a new identifier is randomly generated.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_anon_ind >>

=back

=cut


=construct from_string


B<Signature>:

  RDF::Cowl::AnonInd->from_string()


B<Params>:

  UString $string 


Documentation:


  Returns a retained anonymous individual.
  
  @param string Anonymous individual identifier.
  @return Retained anonymous individual. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_anon_ind_from_string >>

=back

=cut



=method get_id


B<Signature>:

  
  $self->get_id()


B<Params>:

  CowlAnonInd $ind 


Documentation:


  Gets the node ID of the specified anonymous individual.
  
  @param ind The anonymous individual.
  @return Node ID.



=over 2

B<C function name>: C<< cowl_anon_ind_get_id >>

=back

=cut

