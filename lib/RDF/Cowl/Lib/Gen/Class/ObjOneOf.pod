# PODNAME: RDF::Cowl::Lib::Gen::Class::ObjOneOf
# ABSTRACT: Generated docs for RDF::Cowl::ObjOneOf

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::ObjOneOf>

=cut


=construct new


B<Signature>:

  RDF::Cowl::ObjOneOf->new()


B<Params>:

  CowlVector $inds 


Documentation:


  Returns a retained individual enumeration.
  
  @param inds The individuals.
  @return Retained individual enumeration. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_obj_one_of >>

=back

=cut



=method get_inds


B<Signature>:

  
  $self->get_inds()


B<Params>:

  CowlObjOneOf $exp 


Documentation:


  Gets the individuals of the specified enumeration.
  
  @param exp The individual enumeration.
  @return The individuals.



=over 2

B<C function name>: C<< cowl_obj_one_of_get_inds >>

=back

=cut

