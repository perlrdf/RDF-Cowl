# PODNAME: RDF::Cowl::Lib::Gen::Class::ObjHasSelf
# ABSTRACT: Generated docs for RDF::Cowl::ObjHasSelf

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::ObjHasSelf>

=cut


=construct new


B<Signature>:

  RDF::Cowl::ObjHasSelf->new()


B<Params>:

  CowlAnyObjPropExp $prop 


Documentation:


  Returns a retained self-restriction.
  
  @param prop The property expression.
  @return Retained restriction. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_obj_has_self >>

=back

=cut



=method get_prop


B<Signature>:

  
  $self->get_prop()


B<Params>:

  CowlObjHasSelf $exp 


Documentation:


  Gets the object property expression of the specified self-restriction.
  
  @param exp The restriction.
  @return The object property expression.



=over 2

B<C function name>: C<< cowl_obj_has_self_get_prop >>

=back

=cut

