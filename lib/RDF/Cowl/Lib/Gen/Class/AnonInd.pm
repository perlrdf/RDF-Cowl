package RDF::Cowl::Lib::Gen::Class::AnonInd;
# ABSTRACT: Private class for RDF::Cowl::AnonInd

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::AnonInd;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_anon_ind
$ffi->attach( [
 "COWL_WRAP_cowl_anon_ind"
 => "new" ] =>
	[
		arg "CowlString" => "id",
	],
	=> "CowlAnonInd"
	=> sub {
		my $RETVAL;
		my $xs    = shift;
		my $class = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlString, { name => "id", },
			],
		);

		$RETVAL = $xs->( &$signature );

		die "RDF::Cowl::AnonInd::new: error: returned NULL" unless defined $RETVAL;
		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 1;
		return $RETVAL;
	}
);


# cowl_anon_ind_from_string
$ffi->attach( [
 "COWL_WRAP_cowl_anon_ind_from_string"
 => "from_string" ] =>
	[
		arg "UString" => "string",
	],
	=> "CowlAnonInd"
	=> sub {
		my $RETVAL;
		my $xs    = shift;
		my $class = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				UString, { name => "string", },
			],
		);

		$RETVAL = $xs->( &$signature );

		die "RDF::Cowl::AnonInd::from_string: error: returned NULL" unless defined $RETVAL;
		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 1;
		return $RETVAL;
	}
);


# cowl_anon_ind_get_id
$ffi->attach( [
 "COWL_WRAP_cowl_anon_ind_get_id"
 => "get_id" ] =>
	[
		arg "CowlAnonInd" => "ind",
	],
	=> "CowlString"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnonInd, { name => "ind", },
			],
		);

		$RETVAL = $xs->( &$signature );

		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 0;
		$RETVAL = $RETVAL->retain;
		return $RETVAL;
	}
);


1;
