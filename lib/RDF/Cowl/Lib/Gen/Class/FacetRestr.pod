# PODNAME: RDF::Cowl::Lib::Gen::Class::FacetRestr
# ABSTRACT: Generated docs for RDF::Cowl::FacetRestr

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::FacetRestr>

=cut


=construct new


B<Signature>:

  RDF::Cowl::FacetRestr->new(CowlLiteral $value)


B<Params>:

  CowlIRI $facet 
  CowlLiteral $value 


Documentation:


  Returns a retained facet restriction.
  
  @param facet The facet.
  @param value The restriction value.
  @return Retained facet restriction. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_facet_restr >>

=back

=cut



=method get_facet


B<Signature>:

  
  $self->get_facet()


B<Params>:

  CowlFacetRestr $restr 


Documentation:


  Gets the facet.
  
  @param restr The facet restriction.
  @return The facet.



=over 2

B<C function name>: C<< cowl_facet_restr_get_facet >>

=back

=cut



=method get_value


B<Signature>:

  
  $self->get_value()


B<Params>:

  CowlFacetRestr $restr 


Documentation:


  Gets the restriction value.
  
  @param restr The facet restriction.
  @return The restriction value.



=over 2

B<C function name>: C<< cowl_facet_restr_get_value >>

=back

=cut

