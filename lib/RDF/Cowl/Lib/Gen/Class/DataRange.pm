package RDF::Cowl::Lib::Gen::Class::DataRange;
# ABSTRACT: Private class for RDF::Cowl::DataRange

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::DataRange;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_data_range_get_type
$ffi->attach( [
 "COWL_WRAP_cowl_data_range_get_type"
 => "get_type" ] =>
	[
		arg "CowlAnyDataRange" => "range",
	],
	=> "CowlDataRangeType"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnyDataRange, { name => "range", },
			],
		);

		$RETVAL = $xs->( &$signature );

		return $RETVAL;
	}
);


1;
