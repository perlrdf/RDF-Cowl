# PODNAME: RDF::Cowl::Lib::Gen::Class::SubObjPropAxiom
# ABSTRACT: Generated docs for RDF::Cowl::SubObjPropAxiom

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::SubObjPropAxiom>

=cut


=construct new


B<Signature>:

  RDF::Cowl::SubObjPropAxiom->new(CowlAnyObjPropExp $super, Maybe[ CowlVector ] $annot)


B<Params>:

  CowlAnyObjPropExp $sub 
  CowlAnyObjPropExp $super 
  Maybe[ CowlVector ] $annot I<[optional]>


Documentation:


  Returns a retained object subproperty axiom.
  
  @param sub The subproperty.
  @param super The superproperty.
  @param annot [optional] The annotations.
  @return Retained axiom. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_sub_obj_prop_axiom >>

=back

=cut


=construct cowl_sub_obj_prop_chain_axiom


B<Signature>:

  RDF::Cowl::SubObjPropAxiom->cowl_sub_obj_prop_chain_axiom(CowlAnyObjPropExp $super, Maybe[ CowlVector ] $annot)


B<Params>:

  CowlVector $sub 
  CowlAnyObjPropExp $super 
  Maybe[ CowlVector ] $annot I<[optional]>


Documentation:


  Returns a retained object subproperty axiom where the subproperty is a chain of properties.
  
  @param sub The chain of properties.
  @param super The superproperty.
  @param annot [optional] The annotations.
  @return Retained axiom. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_sub_obj_prop_chain_axiom >>

=back

=cut



=method get_sub


B<Signature>:

  
  $self->get_sub()


B<Params>:

  CowlSubObjPropAxiom $axiom 


Documentation:


  Gets the subproperty.
  
  @param axiom The axiom.
  @return The subproperty.
  
  @note The subproperty can be either an object property expression or a property expression chain.



=over 2

B<C function name>: C<< cowl_sub_obj_prop_axiom_get_sub >>

=back

=cut



=method get_super


B<Signature>:

  
  $self->get_super()


B<Params>:

  CowlSubObjPropAxiom $axiom 


Documentation:


  Gets the superproperty.
  
  @param axiom The axiom.
  @return The superproperty.



=over 2

B<C function name>: C<< cowl_sub_obj_prop_axiom_get_super >>

=back

=cut



=method get_annot


B<Signature>:

  
  $self->get_annot()


B<Params>:

  CowlSubObjPropAxiom $axiom 


Documentation:


  Gets the annotations of the specified axiom.
  
  @param axiom The axiom.
  @return The annotations.



=over 2

B<C function name>: C<< cowl_sub_obj_prop_axiom_get_annot >>

=back

=cut

