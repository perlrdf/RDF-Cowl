# PODNAME: RDF::Cowl::Lib::Gen::Class::AnnotProp
# ABSTRACT: Generated docs for RDF::Cowl::AnnotProp

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::AnnotProp>

=cut


=construct new


B<Signature>:

  RDF::Cowl::AnnotProp->new()


B<Params>:

  CowlIRI $iri 


Documentation:


  Returns a retained annotation property.
  
  @param iri IRI of the property.
  @return Retained annotation property. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_annot_prop >>

=back

=cut


=construct from_string


B<Signature>:

  RDF::Cowl::AnnotProp->from_string()


B<Params>:

  UString $string 


Documentation:


  Returns a retained annotation property given the string representation of its IRI.
  
  @param string String representation of the IRI.
  @return Retained annotation property. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_annot_prop_from_string >>

=back

=cut



=method get_iri


B<Signature>:

  
  $self->get_iri()


B<Params>:

  CowlAnnotProp $prop 


Documentation:


  Gets the IRI of the specified annotation property.
  
  @param prop The annotation property.
  @return IRI of the annotation property.



=over 2

B<C function name>: C<< cowl_annot_prop_get_iri >>

=back

=cut

