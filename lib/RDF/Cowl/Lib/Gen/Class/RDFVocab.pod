# PODNAME: RDF::Cowl::Lib::Gen::Class::RDFVocab
# ABSTRACT: Generated docs for RDF::Cowl::RDFVocab

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::RDFVocab>

=cut


=construct new


B<Signature>:

  RDF::Cowl::RDFVocab->new()



Documentation:


  Returns the RDF vocabulary.
  
  @return The RDF vocabulary.



=over 2

B<C function name>: C<< cowl_rdf_vocab >>

=back

=cut

