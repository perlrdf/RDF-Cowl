package RDF::Cowl::Lib::Gen::Class::DataCompl;
# ABSTRACT: Private class for RDF::Cowl::DataCompl

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::DataCompl;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_data_compl
$ffi->attach( [
 "COWL_WRAP_cowl_data_compl"
 => "new" ] =>
	[
		arg "CowlAnyDataRange" => "operand",
	],
	=> "CowlDataCompl"
	=> sub {
		my $RETVAL;
		my $xs    = shift;
		my $class = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnyDataRange, { name => "operand", },
			],
		);

		$RETVAL = $xs->( &$signature );

		die "RDF::Cowl::DataCompl::new: error: returned NULL" unless defined $RETVAL;
		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 1;
		return $RETVAL;
	}
);


# cowl_data_compl_get_operand
$ffi->attach( [
 "COWL_WRAP_cowl_data_compl_get_operand"
 => "get_operand" ] =>
	[
		arg "CowlDataCompl" => "range",
	],
	=> "CowlDataRange"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlDataCompl, { name => "range", },
			],
		);

		$RETVAL = $xs->( &$signature );

		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 0;
		$RETVAL = $RETVAL->retain;
		return $RETVAL;
	}
);


1;
