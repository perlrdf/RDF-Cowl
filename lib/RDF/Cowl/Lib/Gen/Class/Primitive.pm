package RDF::Cowl::Lib::Gen::Class::Primitive;
# ABSTRACT: Private class for RDF::Cowl::Primitive

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::Primitive;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_primitive_get_type
$ffi->attach( [
 "COWL_WRAP_cowl_primitive_get_type"
 => "get_type" ] =>
	[
		arg "CowlAnyPrimitive" => "primitive",
	],
	=> "CowlPrimitiveType"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnyPrimitive, { name => "primitive", },
			],
		);

		$RETVAL = $xs->( &$signature );

		return $RETVAL;
	}
);


# cowl_primitive_is_entity
$ffi->attach( [
 "COWL_WRAP_cowl_primitive_is_entity"
 => "is_entity" ] =>
	[
		arg "CowlAnyPrimitive" => "primitive",
	],
	=> "bool"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnyPrimitive, { name => "primitive", },
			],
		);

		$RETVAL = $xs->( &$signature );

		return $RETVAL;
	}
);


1;
