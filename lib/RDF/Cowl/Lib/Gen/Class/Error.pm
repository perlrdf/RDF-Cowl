package RDF::Cowl::Lib::Gen::Class::Error;
# ABSTRACT: Private class for RDF::Cowl::Error

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::Error;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_error_to_string
$ffi->attach( [
 "COWL_WRAP_cowl_error_to_string"
 => "to_string" ] =>
	[
		arg "CowlError" => "error",
	],
	=> "CowlString"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlError, { name => "error", },
			],
		);

		$RETVAL = $xs->( &$signature );

		die "RDF::Cowl::Error::to_string: error: returned NULL" unless defined $RETVAL;
		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 1;
		return $RETVAL;
	}
);


1;
