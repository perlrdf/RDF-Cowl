# PODNAME: RDF::Cowl::Lib::Gen::Class::ObjCard
# ABSTRACT: Generated docs for RDF::Cowl::ObjCard

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::ObjCard>

=cut


=construct new


B<Signature>:

  RDF::Cowl::ObjCard->new(CowlAnyObjPropExp $prop, Maybe[ CowlAnyClsExp ] $filler, Ulib_uint $cardinality)


B<Params>:

  CowlCardType $type 
  CowlAnyObjPropExp $prop 
  Maybe[ CowlAnyClsExp ] $filler I<[optional]>
  Ulib_uint $cardinality 


Documentation:


  Returns a retained object property cardinality restriction.
  
  @param type The type.
  @param prop The object property.
  @param filler [optional] Filler of the restriction.
  @param cardinality Cardinality of the restriction.
  @return Retained restriction. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_obj_card >>

=back

=cut



=method get_type


B<Signature>:

  
  $self->get_type()


B<Params>:

  CowlObjCard $restr 


Documentation:


  Gets the type of the specified object property cardinality restriction.
  
  @param restr The restriction.
  @return The type.



=over 2

B<C function name>: C<< cowl_obj_card_get_type >>

=back

=cut



=method get_prop


B<Signature>:

  
  $self->get_prop()


B<Params>:

  CowlObjCard $restr 


Documentation:


  Gets the property of the restriction.
  
  @param restr The restriction.
  @return The property.



=over 2

B<C function name>: C<< cowl_obj_card_get_prop >>

=back

=cut



=method get_filler


B<Signature>:

  
  $self->get_filler()


B<Params>:

  CowlObjCard $restr 


Documentation:


  Gets the filler of the restriction.
  
  @param restr The restriction.
  @return The filler.



=over 2

B<C function name>: C<< cowl_obj_card_get_filler >>

=back

=cut



=method get_cardinality


B<Signature>:

  
  $self->get_cardinality()


B<Params>:

  CowlObjCard $restr 


Documentation:


  Gets the cardinality of the restriction.
  
  @param restr The restriction.
  @return The cardinality.



=over 2

B<C function name>: C<< cowl_obj_card_get_cardinality >>

=back

=cut

