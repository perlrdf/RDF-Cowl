package RDF::Cowl::Lib::Gen::Class::SubDataPropAxiom;
# ABSTRACT: Private class for RDF::Cowl::SubDataPropAxiom

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::SubDataPropAxiom;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_sub_data_prop_axiom
$ffi->attach( [
 "COWL_WRAP_cowl_sub_data_prop_axiom"
 => "new" ] =>
	[
		arg "CowlAnyDataPropExp" => "sub",
		arg "CowlAnyDataPropExp" => "super",
		arg "opaque" => "annot",
	],
	=> "CowlSubDataPropAxiom"
	=> sub {
		my $RETVAL;
		my $xs    = shift;
		my $class = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnyDataPropExp, { name => "sub", },
				CowlAnyDataPropExp, { name => "super", },
				Maybe[ CowlVector ], { name => "annot", default => undef, },
			],
		);

		$RETVAL = $xs->( &$signature );

		die "RDF::Cowl::SubDataPropAxiom::new: error: returned NULL" unless defined $RETVAL;
		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 1;
		return $RETVAL;
	}
);


# cowl_sub_data_prop_axiom_get_sub
$ffi->attach( [
 "COWL_WRAP_cowl_sub_data_prop_axiom_get_sub"
 => "get_sub" ] =>
	[
		arg "CowlSubDataPropAxiom" => "axiom",
	],
	=> "CowlDataPropExp"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlSubDataPropAxiom, { name => "axiom", },
			],
		);

		$RETVAL = $xs->( &$signature );

		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 0;
		$RETVAL = $RETVAL->retain;
		return $RETVAL;
	}
);


# cowl_sub_data_prop_axiom_get_super
$ffi->attach( [
 "COWL_WRAP_cowl_sub_data_prop_axiom_get_super"
 => "get_super" ] =>
	[
		arg "CowlSubDataPropAxiom" => "axiom",
	],
	=> "CowlDataPropExp"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlSubDataPropAxiom, { name => "axiom", },
			],
		);

		$RETVAL = $xs->( &$signature );

		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 0;
		$RETVAL = $RETVAL->retain;
		return $RETVAL;
	}
);


# cowl_sub_data_prop_axiom_get_annot
$ffi->attach( [
 "COWL_WRAP_cowl_sub_data_prop_axiom_get_annot"
 => "get_annot" ] =>
	[
		arg "CowlSubDataPropAxiom" => "axiom",
	],
	=> "CowlVector"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlSubDataPropAxiom, { name => "axiom", },
			],
		);

		$RETVAL = $xs->( &$signature );

		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 0;
		$RETVAL = $RETVAL->retain;
		return $RETVAL;
	}
);


1;
