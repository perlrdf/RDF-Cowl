package RDF::Cowl::Lib::Gen::Class::XSDVocab;
# ABSTRACT: Private class for RDF::Cowl::XSDVocab

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::XSDVocab;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_xsd_vocab
$ffi->attach( [
 "COWL_WRAP_cowl_xsd_vocab"
 => "new" ] =>
	[
	],
	=> "CowlXSDVocab"
	=> sub {
		my $RETVAL;
		my $xs    = shift;
		my $class = shift;


		$RETVAL = $xs->( @_ );

		return $RETVAL;
	}
);


1;
