package RDF::Cowl::Lib::Gen::Class::AnnotValue;
# ABSTRACT: Private class for RDF::Cowl::AnnotValue

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::AnnotValue;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_annot_value_get_type
$ffi->attach( [
 "COWL_WRAP_cowl_annot_value_get_type"
 => "get_type" ] =>
	[
		arg "CowlAnyAnnotValue" => "value",
	],
	=> "CowlAnnotValueType"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnyAnnotValue, { name => "value", },
			],
		);

		$RETVAL = $xs->( &$signature );

		return $RETVAL;
	}
);


1;
