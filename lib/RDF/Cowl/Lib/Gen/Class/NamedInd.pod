# PODNAME: RDF::Cowl::Lib::Gen::Class::NamedInd
# ABSTRACT: Generated docs for RDF::Cowl::NamedInd

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::NamedInd>

=cut


=construct new


B<Signature>:

  RDF::Cowl::NamedInd->new()


B<Params>:

  CowlIRI $iri 


Documentation:


  Returns a retained named individual.
  
  @param iri IRI of the individual.
  @return Retained named individual. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_named_ind >>

=back

=cut


=construct from_string


B<Signature>:

  RDF::Cowl::NamedInd->from_string()


B<Params>:

  UString $string 


Documentation:


  Returns a retained named individual given the string representation of its IRI.
  
  @param string String representation of the IRI.
  @return Retained named individual. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_named_ind_from_string >>

=back

=cut



=method get_iri


B<Signature>:

  
  $self->get_iri()


B<Params>:

  CowlNamedInd $ind 


Documentation:


  Gets the IRI of the named individual.
  
  @param ind The named individual.
  @return IRI of the named individual.



=over 2

B<C function name>: C<< cowl_named_ind_get_iri >>

=back

=cut

