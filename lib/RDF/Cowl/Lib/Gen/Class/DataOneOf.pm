package RDF::Cowl::Lib::Gen::Class::DataOneOf;
# ABSTRACT: Private class for RDF::Cowl::DataOneOf

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::DataOneOf;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_data_one_of
$ffi->attach( [
 "COWL_WRAP_cowl_data_one_of"
 => "new" ] =>
	[
		arg "CowlVector" => "values",
	],
	=> "CowlDataOneOf"
	=> sub {
		my $RETVAL;
		my $xs    = shift;
		my $class = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlVector, { name => "values", },
			],
		);

		$RETVAL = $xs->( &$signature );

		die "RDF::Cowl::DataOneOf::new: error: returned NULL" unless defined $RETVAL;
		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 1;
		return $RETVAL;
	}
);


# cowl_data_one_of_get_values
$ffi->attach( [
 "COWL_WRAP_cowl_data_one_of_get_values"
 => "get_values" ] =>
	[
		arg "CowlDataOneOf" => "range",
	],
	=> "CowlVector"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlDataOneOf, { name => "range", },
			],
		);

		$RETVAL = $xs->( &$signature );

		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 0;
		$RETVAL = $RETVAL->retain;
		return $RETVAL;
	}
);


1;
