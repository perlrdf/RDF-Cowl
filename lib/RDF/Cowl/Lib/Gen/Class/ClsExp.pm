package RDF::Cowl::Lib::Gen::Class::ClsExp;
# ABSTRACT: Private class for RDF::Cowl::ClsExp

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::ClsExp;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_cls_exp_get_type
$ffi->attach( [
 "COWL_WRAP_cowl_cls_exp_get_type"
 => "get_type" ] =>
	[
		arg "CowlAnyClsExp" => "exp",
	],
	=> "CowlClsExpType"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnyClsExp, { name => "exp", },
			],
		);

		$RETVAL = $xs->( &$signature );

		return $RETVAL;
	}
);


1;
