# PODNAME: RDF::Cowl::Lib::Gen::Class::ObjCompl
# ABSTRACT: Generated docs for RDF::Cowl::ObjCompl

## DO NOT EDIT
## Generated via maint/tt/Class.pod.tt

=pod

=head1 MAIN MODULE

L<RDF::Cowl::ObjCompl>

=cut


=construct new


B<Signature>:

  RDF::Cowl::ObjCompl->new()


B<Params>:

  CowlAnyClsExp $operand 


Documentation:


  Returns a retained class expression complement.
  
  @param operand The operand.
  @return Retained class expression complement. Throws exception on error.


Throws exception on error.


=over 2

B<C function name>: C<< cowl_obj_compl >>

=back

=cut



=method get_operand


B<Signature>:

  
  $self->get_operand()


B<Params>:

  CowlObjCompl $exp 


Documentation:


  Gets the operand of the specified class expression complement.
  
  @param exp The complement.
  @return The operand.



=over 2

B<C function name>: C<< cowl_obj_compl_get_operand >>

=back

=cut

