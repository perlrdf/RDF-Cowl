package RDF::Cowl::Lib::Gen::Class::Entity;
# ABSTRACT: Private class for RDF::Cowl::Entity

## DO NOT EDIT
## Generated via maint/tt/Class.pm.tt

package # hide from PAUSE
  RDF::Cowl::Entity;

use strict;
use warnings;
use feature qw(state);
use Devel::StrictMode qw( STRICT );
use RDF::Cowl::Lib qw(arg);
use RDF::Cowl::Lib::Types qw(:all);
use Types::Common qw(Maybe BoolLike PositiveOrZeroInt Str StrMatch InstanceOf);
use Type::Params -sigs;

my $ffi = RDF::Cowl::Lib->ffi;


# cowl_entity_get_type
$ffi->attach( [
 "COWL_WRAP_cowl_entity_get_type"
 => "get_type" ] =>
	[
		arg "CowlAnyEntity" => "entity",
	],
	=> "CowlEntityType"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnyEntity, { name => "entity", },
			],
		);

		$RETVAL = $xs->( &$signature );

		return $RETVAL;
	}
);


# cowl_entity_get_iri
$ffi->attach( [
 "COWL_WRAP_cowl_entity_get_iri"
 => "get_iri" ] =>
	[
		arg "CowlAnyEntity" => "entity",
	],
	=> "CowlIRI"
	=> sub {
		my $RETVAL;
		my $xs    = shift;


		state $signature = signature(
			strictness => STRICT,
			pos => [
				CowlAnyEntity, { name => "entity", },
			],
		);

		$RETVAL = $xs->( &$signature );

		$RDF::Cowl::Object::_INSIDE_OUT{$$RETVAL}{retained} = 0;
		$RETVAL = $RETVAL->retain;
		return $RETVAL;
	}
);


1;
