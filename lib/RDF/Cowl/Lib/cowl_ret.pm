package RDF::Cowl::Lib::cowl_ret;
# ABSTRACT: cowl_ret status used for error checking

use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum cowl_ret
# From <cowl_ret.h>

my @ENUM_CODES = qw(
    COWL_OK
    COWL_ERR
    COWL_ERR_IO
    COWL_ERR_MEM
    COWL_ERR_SYNTAX
    COWL_ERR_IMPORT
);
my @_COWL_RET_CODE =
	map {
		[ $ENUM_CODES[$_] =~ s/^COWL_//r , $_ ],
	} 0..$#ENUM_CODES;

$ffi->load_custom_type('::Enum', 'cowl_ret',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_RET_CODE,
);

1;
