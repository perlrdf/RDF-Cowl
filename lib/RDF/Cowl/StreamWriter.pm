package RDF::Cowl::StreamWriter;
# ABSTRACT: Defines functions that must be implemented by stream writers

# CowlStreamWriter
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);
use FFI::Platypus::Record;

my $ffi = RDF::Cowl::Lib->ffi;
record_layout_1($ffi,
	# cowl_ret (*write_header)(UOStream *stream, CowlSymTable *st, CowlOntologyHeader header);
	opaque => 'write_header',

	# cowl_ret (*write_axiom)(UOStream *stream, CowlSymTable *st, CowlAnyAxiom *axiom);
	opaque => 'write_axiom',

	# cowl_ret (*write_footer)(UOStream *stream, CowlSymTable *st);
	opaque => 'write_footer',
);
$ffi->type('record(RDF::Cowl::StreamWriter)', 'CowlStreamWriter');

1;
