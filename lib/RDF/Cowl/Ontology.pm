package RDF::Cowl::Ontology;
# ABSTRACT: Represents an Ontology in the OWL 2 specification

# CowlOntology
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Ontology unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Ontology

=cut
