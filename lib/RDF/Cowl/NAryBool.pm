package RDF::Cowl::NAryBool;
# ABSTRACT: Represents ObjectIntersectionOf and ObjectUnionOf in the OWL 2 specification

# CowlNAryBool
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::NAryBool unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc NAryBool

=cut
