package RDF::Cowl::AnnotAssertAxiom;
# ABSTRACT: Represents an AnnotationAssertion axiom in the OWL 2 specification

# CowlAnnotAssertAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::AnnotAssertAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc AnnotAssertAxiom

=cut
