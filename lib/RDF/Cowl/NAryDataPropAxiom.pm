package RDF::Cowl::NAryDataPropAxiom;
# ABSTRACT: Represents an EquivalentDataProperties or DisjointDataProperties axiom in the OWL 2 specification

# CowlNAryDataPropAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::NAryDataPropAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc NAryDataPropAxiom

=cut
