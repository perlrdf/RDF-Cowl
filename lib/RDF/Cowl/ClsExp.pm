package RDF::Cowl::ClsExp;
# ABSTRACT: Represents a ClassExpression in the OWL 2 specification

# CowlClsExp
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ClsExp unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ClsExp

=cut
