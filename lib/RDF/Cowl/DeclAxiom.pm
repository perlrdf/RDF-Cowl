package RDF::Cowl::DeclAxiom;
# ABSTRACT: Represents a Declaration in the OWL 2 specification

# CowlDeclAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DeclAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DeclAxiom

=cut
