package RDF::Cowl::ClsAssertAxiom;
# ABSTRACT: Represents a ClassAssertion axiom in the OWL 2 specification

# CowlClsAssertAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ClsAssertAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ClsAssertAxiom

=cut
