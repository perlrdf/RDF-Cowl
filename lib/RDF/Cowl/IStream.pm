package RDF::Cowl::IStream;
# ABSTRACT: Ontology input stream

# CowlIStream
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::IStream unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc IStream

=cut
