package RDF::Cowl::EntityType;
# ABSTRACT: Represents the type of CowlEntity

# CowlEntityType
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum CowlEntityType
# From <cowl_entity_type.h>

my @ENUM_CODES = qw(
    COWL_ET_CLASS
    COWL_ET_OBJ_PROP
    COWL_ET_DATA_PROP
    COWL_ET_ANNOT_PROP
    COWL_ET_NAMED_IND
    COWL_ET_DATATYPE
);
my @_COWL_ET_CODE =
	map {
		[ $ENUM_CODES[$_] =~ s/^COWL_ET_//r , $_ ],
	} 0..$#ENUM_CODES;

$ffi->load_custom_type('::Enum', 'CowlEntityType',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_ET_CODE,
);


1;
