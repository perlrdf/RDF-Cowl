package RDF::Cowl::DataCompl;
# ABSTRACT: Represents DataComplementOf the OWL 2 specification

# CowlDataCompl
use strict;
use warnings;
use parent 'RDF::Cowl::DataRange';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataCompl unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataCompl

=cut
