package RDF::Cowl::ObjHasValue;
# ABSTRACT: Represents ObjectHasValue in the OWL 2 specification

# CowlObjHasValue
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjHasValue unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjHasValue

=cut
