package RDF::Cowl::NAryType;
# ABSTRACT: Represents the type of CowlNAryBool and CowlNAryData

# CowlNAryType
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum CowlNAryType
# From <cowl_nary_type.h>

my @ENUM_CODES = qw(
    COWL_NT_INTERSECT
    COWL_NT_UNION
);
my @_COWL_NT_CODE =
	map {
		[ $ENUM_CODES[$_] =~ s/^COWL_NT_//r , $_ ],
	} 0..$#ENUM_CODES;

$ffi->load_custom_type('::Enum', 'CowlNAryType',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_NT_CODE,
);


1;
