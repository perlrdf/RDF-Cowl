package RDF::Cowl::Individual;
# ABSTRACT: Represents an Individual in the OWL 2 specification

# CowlIndividual
use strict;
use warnings;
use parent 'RDF::Cowl::Primitive';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Individual unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Individual

=cut
