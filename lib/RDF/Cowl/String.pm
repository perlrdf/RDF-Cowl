package RDF::Cowl::String;
# ABSTRACT: The string type

# CowlString
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

use overload '""' => 'STRINGIFY';

sub STRINGIFY {
	my ($self) = @_;
	$self->get_cstring;
}

# cowl_string_get_cstring
$ffi->attach( [ "cowl_string_get_cstring" => "get_cstring" ] =>
	[
		arg "CowlString" => "string",
	],
	=> "opaque"
	=> sub {
		my $RETVAL;
		my $xs    = shift;
		$RETVAL = $xs->(@_);

		# TODO maybe use window() instead
		# This copies the string to Perl
		my $RETVAL_str = $ffi->cast( 'opaque' => 'string', $RETVAL );

		return $RETVAL_str;
	}
);

require RDF::Cowl::Lib::Gen::Class::String unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc String

=cut
