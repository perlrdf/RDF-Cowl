package RDF::Cowl::OStream;
# ABSTRACT: Ontology output stream

# CowlOStream
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::OStream unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc OStream

=cut
