package RDF::Cowl::ObjCard;
# ABSTRACT: Represents ObjectMinCardinality, ObjectMaxCardinality and ObjectExactCardinality in the OWL 2 specification

# CowlObjCard
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjCard unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjCard

=cut
