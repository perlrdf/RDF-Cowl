package RDF::Cowl::AnnotPropRangeAxiom;
# ABSTRACT: Represents an AnnotationPropertyRange axiom in the OWL 2 specification

# CowlAnnotPropRangeAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::AnnotPropRangeAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc AnnotPropRangeAxiom

=cut
