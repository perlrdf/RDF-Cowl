package RDF::Cowl::Vector;
# ABSTRACT: Vector of RDF::Cowl::Object elements

# CowlVector
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Vector unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Vector

=cut
