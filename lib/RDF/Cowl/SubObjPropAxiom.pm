package RDF::Cowl::SubObjPropAxiom;
# ABSTRACT: Represents a SubObjectPropertyOf axiom in the OWL 2 specification

# CowlSubObjPropAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::SubObjPropAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc SubObjPropAxiom

=cut
