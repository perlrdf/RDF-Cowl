package RDF::Cowl::DisjUnionAxiom;
# ABSTRACT: Represents a DisjointUnion axiom in the OWL 2 specification

# CowlDisjUnionAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DisjUnionAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DisjUnionAxiom

=cut
