package RDF::Cowl::AnonInd;
# ABSTRACT: Represents an AnonymousIndividual in the OWL 2 specification

# CowlAnonInd
use strict;
use warnings;
use parent qw(RDF::Cowl::Individual RDF::Cowl::Primitive RDF::Cowl::AnnotValue);
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::AnonInd unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc AnonInd

=cut
