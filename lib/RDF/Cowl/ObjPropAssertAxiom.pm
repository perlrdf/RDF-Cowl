package RDF::Cowl::ObjPropAssertAxiom;
# ABSTRACT: Represents an ObjectPropertyAssertion or NegativeObjectPropertyAssertion axiom in the OWL 2 specification

# CowlObjPropAssertAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjPropAssertAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjPropAssertAxiom

=cut
