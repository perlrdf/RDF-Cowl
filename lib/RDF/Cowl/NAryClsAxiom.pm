package RDF::Cowl::NAryClsAxiom;
# ABSTRACT: Represents an EquivalentClasses or DisjointClasses axiom in the OWL 2 specification

# CowlNAryClsAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::NAryClsAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc NAryClsAxiom

=cut
