package RDF::Cowl::DatatypeRestr;
# ABSTRACT: Represents a DatatypeRestriction in the OWL 2 specification

# CowlDatatypeRestr
use strict;
use warnings;
use parent 'RDF::Cowl::DataRange';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DatatypeRestr unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DatatypeRestr

=cut
