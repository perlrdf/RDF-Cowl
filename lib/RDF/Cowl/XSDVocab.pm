package RDF::Cowl::XSDVocab;
# ABSTRACT: The XSD vocabulary

# CowlXSDVocab
use strict;
use warnings;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::XSDVocab unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc XSDVocab

=cut
