package RDF::Cowl::NAryData;
# ABSTRACT: Represents DataIntersectionOf and DataUnionOf in the OWL 2 specification

# CowlNAryData
use strict;
use warnings;
use parent 'RDF::Cowl::DataRange';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::NAryData unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc NAryData

=cut
