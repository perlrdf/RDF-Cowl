package RDF::Cowl::SubClsAxiom;
# ABSTRACT: Represents a SubClassOf axiom in the OWL 2 specification

# CowlSubClsAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::SubClsAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc SubClsAxiom

=cut
