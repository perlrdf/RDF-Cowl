package RDF::Cowl::ObjHasSelf;
# ABSTRACT: Represents ObjectHasSelf in the OWL 2 specification

# CowlObjHasSelf
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjHasSelf unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjHasSelf

=cut
