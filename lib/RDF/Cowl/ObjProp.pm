package RDF::Cowl::ObjProp;
# ABSTRACT: Represents an ObjectProperty in the OWL 2 specification

# CowlObjProp
use strict;
use warnings;
use parent qw(RDF::Cowl::ObjPropExp RDF::Cowl::Entity);
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjProp unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjProp

=cut
