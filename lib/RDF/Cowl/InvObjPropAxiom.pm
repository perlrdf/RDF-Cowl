package RDF::Cowl::InvObjPropAxiom;
# ABSTRACT: Represents an InverseObjectProperties axiom in the OWL 2 specification

# CowlInvObjPropAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::InvObjPropAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc InvObjPropAxiom

=cut
