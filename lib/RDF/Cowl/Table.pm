package RDF::Cowl::Table;
# ABSTRACT: Hash table of RDF::Cowl::Object elements

# CowlTable
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Table unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Table

=cut
