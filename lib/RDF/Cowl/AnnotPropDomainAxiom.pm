package RDF::Cowl::AnnotPropDomainAxiom;
# ABSTRACT: Represents an AnnotationPropertyDomain axiom in the OWL 2 specification

# CowlAnnotPropDomainAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::AnnotPropDomainAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc AnnotPropDomainAxiom

=cut
