package RDF::Cowl::Axiom;
# ABSTRACT: Represents an Axiom in the OWL 2 specification

# CowlAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Axiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Axiom

=cut
