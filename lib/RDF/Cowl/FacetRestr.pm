package RDF::Cowl::FacetRestr;
# ABSTRACT: Represents a facet restriction used to restrict a particular CowlDatatype

# CowlFacetRestr
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::FacetRestr unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc FacetRestr

=cut
