package RDF::Cowl::ObjQuant;
# ABSTRACT: Represents ObjectSomeValuesFrom and ObjectAllValuesFrom in the OWL 2 specification

# CowlObjQuant
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjQuant unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjQuant

=cut
