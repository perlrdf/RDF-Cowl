package RDF::Cowl::SymTable;
# ABSTRACT: Maps symbols to OWL constructs

# CowlSymTable
use strict;
use warnings;
use parent 'RDF::Cowl::Object'; # NOTE not in documentation
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::SymTable unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc SymTable

=cut
