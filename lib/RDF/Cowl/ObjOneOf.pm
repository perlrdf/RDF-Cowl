package RDF::Cowl::ObjOneOf;
# ABSTRACT: Represents ObjectOneOf in the OWL 2 specification

# CowlObjOneOf
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjOneOf unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjOneOf

=cut
