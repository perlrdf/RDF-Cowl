package RDF::Cowl::NAryIndAxiom;
# ABSTRACT: Represents a SameIndividuals or DifferentIndividuals axiom in the OWL 2 specification

# CowlNAryIndAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::NAryIndAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc NAryIndAxiom

=cut
