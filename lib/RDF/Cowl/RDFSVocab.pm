package RDF::Cowl::RDFSVocab;
# ABSTRACT: The RDFS vocabulary

# CowlRDFSVocab
use strict;
use warnings;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::RDFSVocab unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc RDFSVocab

=cut
