package RDF::Cowl::DataCard;
# ABSTRACT: Represents DataMinCardinality, DataMaxCardinality and DataExactCardinality in the OWL 2 specification

# CowlDataCard
use strict;
use warnings;
use parent 'RDF::Cowl::ClsExp';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataCard unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataCard

=cut
