package RDF::Cowl::NamedInd;
# ABSTRACT: Represents a NamedIndividual in the OWL 2 specification

# CowlNamedInd
use strict;
use warnings;
use parent qw(RDF::Cowl::Individual RDF::Cowl::Entity);
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::NamedInd unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc NamedInd

=cut
