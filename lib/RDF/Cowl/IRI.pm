package RDF::Cowl::IRI;
# ABSTRACT: Represents International Resource Identifiers

# CowlIRI
use strict;
use warnings;
use parent 'RDF::Cowl::AnnotValue';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::IRI unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc IRI

=cut
