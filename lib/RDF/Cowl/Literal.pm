package RDF::Cowl::Literal;
# ABSTRACT: Represents a Literal in the OWL 2 specification

# CowlLiteral
use strict;
use warnings;
use parent 'RDF::Cowl::AnnotValue';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Literal unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Literal

=cut
