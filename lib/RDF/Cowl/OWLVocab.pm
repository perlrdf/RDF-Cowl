package RDF::Cowl::OWLVocab;
# ABSTRACT: The OWL 2 vocabulary

# CowlOWLVocab
use strict;
use warnings;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::OWLVocab unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc OWLVocab

=cut
