package RDF::Cowl::ObjPropCharAxiom;
# ABSTRACT: Represents a FunctionalObjectProperty, InverseFunctionalObjectProperty, SymmetricObjectProperty, AsymmetricObjectProperty, TransitiveObjectProperty, ReflexiveObjectProperty or IrreflexiveObjectProperty axiom in the OWL 2 specification

# CowlObjPropCharAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjPropCharAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjPropCharAxiom

=cut
