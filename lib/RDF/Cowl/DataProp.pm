package RDF::Cowl::DataProp;
# ABSTRACT: Represents a DataProperty in the OWL 2 specification

# CowlDataProp
use strict;
use warnings;
use parent qw(RDF::Cowl::DataPropExp RDF::Cowl::Entity);
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::DataProp unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc DataProp

=cut
