package RDF::Cowl::CardType;
# ABSTRACT: Represents the type of CowlObjCard and CowlDataCard

# CowlCardType
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum CowlCardType
# From <cowl_card_type.h>

my @ENUM_CODES = qw(
    COWL_CT_MIN
    COWL_CT_MAX
    COWL_CT_EXACT
);
my @_COWL_CT_CODE =
	map {
		[ $ENUM_CODES[$_] =~ s/^COWL_CT_//r , $_ ],
	} 0..$#ENUM_CODES;

$ffi->load_custom_type('::Enum', 'CowlCardType',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_CT_CODE,
);


1;
