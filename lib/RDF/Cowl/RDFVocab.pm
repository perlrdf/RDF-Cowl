package RDF::Cowl::RDFVocab;
# ABSTRACT: The RDF vocabulary

# CowlRDFVocab
use strict;
use warnings;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::RDFVocab unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc RDFVocab

=cut
