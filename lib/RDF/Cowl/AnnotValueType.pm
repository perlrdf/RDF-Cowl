package RDF::Cowl::AnnotValueType;
# ABSTRACT: Represents the type of CowlAnnotValue

# CowlAnnotValueType
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib;
use FFI::C;

my $ffi = RDF::Cowl::Lib->ffi;

# enum CowlAnnotValueType
# From <cowl_annot_value_type.h>

my @ENUM_CODES = qw(
    COWL_AVT_IRI
    COWL_AVT_LITERAL
    COWL_AVT_ANON_IND
);
my @_COWL_AVT_CODE =
	map {
		[ $ENUM_CODES[$_] =~ s/^COWL_AVT_//r , $_ ],
	} 0..$#ENUM_CODES;

$ffi->load_custom_type('::Enum', 'CowlAnnotValueType',
	{ rev => 'int', package => __PACKAGE__ },
	@_COWL_AVT_CODE,
);


1;
