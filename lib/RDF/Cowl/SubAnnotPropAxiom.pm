package RDF::Cowl::SubAnnotPropAxiom;
# ABSTRACT: Represents a SubAnnotationPropertyOf axiom in the OWL 2 specification

# CowlSubAnnotPropAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::SubAnnotPropAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc SubAnnotPropAxiom

=cut
