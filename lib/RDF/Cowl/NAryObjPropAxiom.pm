package RDF::Cowl::NAryObjPropAxiom;
# ABSTRACT: Represents an EquivalentObjectProperties or DisjointObjectProperties axiom in the OWL 2 specification

# CowlNAryObjPropAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::NAryObjPropAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc NAryObjPropAxiom

=cut
