package RDF::Cowl::Error;
# ABSTRACT: Error data structure

# CowlError
use strict;
use warnings;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::Error unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Error

=cut
