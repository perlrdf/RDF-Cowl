package RDF::Cowl::Manager;
# ABSTRACT: Manages ontology documents

# CowlManager
use strict;
use warnings;
use parent 'RDF::Cowl::Object';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

$ffi->attach( [ "cowl_manager_write_file" => "write_FILE" ] =>
	[
		arg "CowlManager" => "manager",
		arg "CowlOntology" => "ontology",
		arg "FILE" => "file",
	],
	=> "cowl_ret"
);

$ffi->attach( [ "cowl_manager_read_file" => "read_FILE" ] =>
	[
		arg "CowlManager" => "manager",
		arg "FILE" => "file",
	],
	=> "CowlOntology"
);

require RDF::Cowl::Lib::Gen::Class::Manager unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc Manager

=cut
