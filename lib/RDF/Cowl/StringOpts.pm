package RDF::Cowl::StringOpts;
# ABSTRACT: String creation options

# CowlStringOpts
use strict;
use warnings;
use namespace::autoclean;
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;
## #define COWL_SO 8
$ffi->type('uint8_t', 'CowlStringOpts');

use Const::Exporter
opts => [
	NONE   => 0,
	COPY   => 1<<0,
	INTERN => 1<<1,
];

use Const::Exporter
aggregate => [
	NONE => 0,
];


1;
