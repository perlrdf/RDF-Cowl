package RDF::Cowl::ObjPropRangeAxiom;
# ABSTRACT: Represents an ObjectPropertyRange axiom in the OWL 2 specification

# CowlObjPropRangeAxiom
use strict;
use warnings;
use parent 'RDF::Cowl::Axiom';
use RDF::Cowl::Lib qw(arg);

my $ffi = RDF::Cowl::Lib->ffi;

require RDF::Cowl::Lib::Gen::Class::ObjPropRangeAxiom unless $RDF::Cowl::no_gen;

1;
__END__

=pod

=cowl_gendoc ObjPropRangeAxiom

=cut
